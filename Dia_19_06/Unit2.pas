unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent, Vcl.StdCtrls, Vcl.Menus;

type
  TForm2 = class(TForm)
    navegador: TNetHTTPClient;
    Button1: TButton;
    ListBox1: TListBox;
    procedure Button1Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TPersonagens= class(TObject)
    public
     nome   :string;
     classe :string;
     nivel  :Integer;
     ataque :Integer;
     defesa :Integer;
     vida   :Integer;
  end;



var
  Form2: TForm2;
  TPersonagem : TPersonagens;

implementation

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
var conteudo                : string;
    listaPersonagens        : TArray<String>;
    listaPessoas            : TArray<String>;
    stringPersonagem        : String;
    perso                   : TPersonagens;
begin
    ListBox1.Clear;
    conteudo := navegador.Get('https://venson.net.br/ws/personagens').ContentAsString();

    listaPersonagens := conteudo.Split(['&']);

    for stringPersonagem in listaPersonagens do
    begin

        perso          := TPersonagem.Create();
        listaPessoas   := stringPersonagem.Split([';']);
        perso.nome     := listaPessoas[0];
        perso.classe   := listaPessoas[1];
        perso.nivel    := listaPessoas[2].ToInteger;
        perso.ataque   := listaPessoas[3].ToInteger;
        perso.defesa   := listaPessoas[4].ToInteger;
        perso.vida     := listaPessoas[5].ToInteger;
        ListBox1.Items.AddObject(perso.nome, perso);
    end;

end;

procedure TForm2.ListBox1Click(Sender: TObject);
var  personagens : TPersonagens;
begin
   personagens :=ListBox1.Items.Objects[ListBox1.ItemIndex] as TPersonagens;
   ShowMessage(personagens.nome+' � da classe '+personagens.classe);
end;

end.
